@extends('admin.tpl.master')

@section('title')
  Settings - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>Settings</h1>
		<p>Set the software settings here.</p>
	</div>


    @if ($errors->count() > 0)
      <ul class="alert alert-error">
          {{ $errors->first('LEAVE_EMAIL', '<li>:message</li>') }}
      </ul> 
    @endif

    @if (Session::has('error'))
        <p class="alert alert-error"> {{ Session::get('error') }} </p>
    @elseif ( Session::has('success') )
      <p class="alert alert-success"> {{ Session::get('success') }} </p>
    @endif

	{{ Form::open() }}   

	@foreach ( $settings as $setting )
	
		<div class="control-group">
			
			@if($setting['name'] == "LEAVE_EMAIL")
				<label>Leave Email</label>
			@endif

			<div class="controls">
				{{ Form::text($setting['name'], $setting['value'], array('placeholder' => 'Email') ) }}
				@if($setting['name'] == "LEAVE_EMAIL")<span class="help-inline">This will receive all the leave applications</span> @endif
			</div>
		</div>

	@endforeach

	{{ Form::submit('submit', array('id' => 'submit', 'class' => 'btn btn-primary')) }}

	{{ Form::close() }}

@stop