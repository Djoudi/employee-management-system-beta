@extends('admin.tpl.master')

@section('title')
  File a leave - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>File a leave request</h1>
		<p>Please fill up the form for your desired leave details.</p>
	</div>

	{{ Form::open(array('class' => 'vertical')) }}

        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('leave_details', '<li>:message</li>') }}
          </ul> 
        @endif

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif

        <div id="alert" class="alert alert-error" style="display:none">
        	<strong></strong>
        </div>

		<div class="control-group span8">
			<label for="leave_type">Type of Leave</label>
			<div class="controls">
				{{ Form::select('leave_type', array('annual' => 'Annual / Vacation Leave', 'sick' => 'Sick / Medical Leave', 'maternity' => 'Maternity Leave', 'other' => 'Other'), 'annual', array('class' => 'input-xlarge', 'id' => 'leave_type')) }}
			</div>
			<div class="controls other_reason hide">
				<label for="other_reason">Other Reason</label>
				{{ Form::text('other_reason', Input::old('other_reason'), array('id' => 'other_reason', 'class' => 'input-xlarge', 'placeholder' => 'Other Reason', 'rows' => '5') ) }}				
			</div>
		</div>

		<div class="control-group span8">
			<label for="day_leave_type" class="control-label">Day Leave Type</label>
			<div class="controls">
				{{ Form::select('day_leave_type', array('#noSelect' => '--Select--', 'halfday' => 'Half Day', 'oneday' => 'One Day', 'multiple' => 'Multiple days'), '#noSelect', array('id' => 'day_leave_type', 'class' => 'input-xlarge') ) }}
			</div>
		</div>

		<div id="leave_time_wrap" class="control-group span8 hide">
			<label for="leave_time" class="control-label">Time of the day</label>
			<div class="controls">
				{{ Form::select('leave_time', array('#noSelect' => '--Select--','morning' => 'Morning', 'afternoon' => 'Afternoon'), '#noSelect', array('id' => 'leave_time', 'class' => 'input-xlarge', 'disabled' => true) ) }}
			</div>
		</div>				

		<div id="single_day" class="control-group hide span8">
			<fieldset class="pull-left">
				<label class="control-label">Date</label>
				<div class="controls">
					<div class="input-append date" data-date="{{ date("Y-m-d") }}" data-date-format="yyyy-mm-dd">
					  <input name="day_leave" id="day_leave" class="input-xlarge" size="16" type="text" value="{{ date("Y-m-d") }}" disabled>			  
					  <span class="add-on"><i class="icon-th"></i></span>
					</div>				      					
				</div>
			</fieldset>
		</div>

		<div id="multiple_days" class="control-group hide span8">
			<fieldset class="pull-left">
				<label class="control-label" for="date_from">Date From:</label>
				<div class="controls">
					<div class="input-append date" data-date="{{ date("Y-m-d") }}" data-date-format="yyyy-mm-dd">
					  <input name="date_from" id="date_from" class="input-xlarge multiple_days_input" size="16" type="text" value="{{ date("Y-m-d") }}" disabled>			  
					  <span class="add-on"><i class="icon-th"></i></span>
					</div>				      					
				</div>
			</fieldset>

			<fieldset class="pull-left span3">
				<label class="control-label" for="date_to">Date To:</label>
				<div class="controls">
					<div class="input-append date date_to" data-date="{{ date("Y-m-d", strtotime("now +1 day")) }}" data-date-format="yyyy-mm-dd">
					  <input name="date_to" id="date_to" data-status="disabled" class="input-xlarge multiple_days_input" size="16" type="text" value="{{ date("Y-m-d", strtotime("now +1 day")) }}" disabled>
					  <span class="add-on"><i class="icon-th"></i></span>
					</div>				      					
				</div>
			</fieldset>
		</div>

		<div class="control-group span8">
			<label for="leave_details">Details of the leave <sup class="text-error">*</sup></label>
			<div class="controls">
				{{ Form::textarea('leave_details', Input::old('leave_details'), array('id' => 'leave_details', 'class' => 'input-xlarge', 'placeholder' => 'Details'))}}
			</div>
		</div>

		<div class="control-group clear span8">
			<div class="controls btn-group">				
				<button class="btn btn-primary" onClick="return confirm('Send this leave request?');">Send request</button>
			</div>
		</div>


	{{ Form::close() }}
@stop