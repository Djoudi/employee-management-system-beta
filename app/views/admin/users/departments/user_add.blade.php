@extends('admin.tpl.master')

@section('title')
  Add user to department - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>Add user to department</h1>
			<p>add user to department</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif        

        {{ Form::open() }}
		
		{{ Form::hidden('id_department', $department->id_department) }}
		{{ Form::hidden('name', false, array('id' => 'name')) }}
		{{ Form::hidden('department_name', false, array('id' => 'department_name')) }}		
		{{ Form::hidden('department_role_name', false, array('id' => 'department_role_name')) }}
		
		<div class="control-group">
			<label class="control-label">Department Name </label>
			<div id="id_department" class="controls">
				{{ $department->name }}
			</div>
		</div>

		<div class="control-group">
			<label for="id_user" class="control-label">Add this user</label>
			<div class="controls">
				<select name="id_user" id="id_user" class="input-xlarge">
					@foreach ( $users as $user )
						<option value="{{ $user->id_user }}">{{ $user->last_name }}, {{ $user->first_name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		
		<div class="control-group">
			<label for="id_department_role" class="control-label">To which department role?</label>
			<div class="controls">
				<select name="id_department_role" id="id_department_role" class="input-xlarge">						
					@foreach ( $department_roles as $department_role )
						<option value="{{ $department_role->id_department_role}}" data-name="{{ $department_role->name }}">{{ $department_role->name }}</option>
					@endforeach
				</select>				
			</div>
		</div>
		
		<div class="control-group submit_button">
			<button class="btn btn-primary input-xlarge" id="department_user_add">Add user</button>
		</div>

		{{ Form::close() }}

		<script type="text/javascript">
			$(document).ready(function(){

				$('#name').val($('#id_user option:selected').html());				
				$('#department_role_name').val($('#id_department_role option:selected').html());
				$('#department_name').val( $.trim( $('#id_department').text()) );				

				$('#id_user').on('change', function(){
					var selected = $('#id_user option:selected').html();
					//PUT IT IN THE DEPARTMENT ROLE NAME ID
					$('#name').val(selected);
				});

				$('#id_department_role').on('change', function(){
					var selected = $('#id_department_role option:selected').html();
					//PUT IT IN THE DEPARTMENT ROLE NAME ID
					$('#department_role_name').val(selected);
				});

			});
		</script>		
@stop