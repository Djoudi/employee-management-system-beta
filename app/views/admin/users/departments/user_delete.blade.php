@extends('admin.tpl.master')

@section('title')
  Edit user to department - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>Edit user to department</h1>
			<p>edit user to department</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif 

        {{ Form::open() }}

		<div class="control-group">
			<label for="id_user" class="control-label">Name</label>
			<div class="controls">
				{{ $users->last_name }}, {{ $users->first_name }}
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Department </label>
			<div class="controls">
				{{ $departments->name }}				
			</div>
		</div>		
		
		<div class="control-group">
			<label for="id_department_role" class="control-label">Department role</label>
			<div class="controls">
				{{ $department_roles->name }}
			</div>
		</div>

		<div class="control-group submit_button">
			<button class="btn btn-danger input-xlarge" id="department_user_delete" onClick="return confirm('Delete this user in this role/department?');">Delete user</button>
		</div>

		{{ Form::hidden('department_name', '-') }}
		{{ Form::hidden('department_role_name', '-') }}
		
		{{ Form::close() }}
@stop