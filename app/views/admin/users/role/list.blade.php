@extends('admin.tpl.master')

@section('title')
  Users Role - Employee Management and Leave System
@stop

@section('content')
	
	<div class="page-header">
		<h1>Users Role</h1>
		<p>Displays all the users role.</p>
	</div>

@if (count($records) > 0)
    
    @if (Session::has('message'))
        <p class="alert alert-success"> {{ Session::get('message') }} </p>
    @endif

    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Role</th>
          <th class="span5">Description</th>
          <th class="span3">Action</th>          
        </tr>
      </thead>
      <tbody>      	

			@foreach ($records as $role)
		        <tr>
		          <td>{{ $role->id_role }}</td>
		          <td>{{ $role->role }} </td>
              <td>{{ $role->role_description }} </td>
              <td>
                <a title="Edit Role" class="btn btn-primary" href="{{ URL::to("admin/users/role/update/$role->id_role") }}"><i class="icon-pencil icon-white"></i></a>
                <a title="Delete Role" class="btn btn-danger" href="{{ URL::to("admin/users/role/delete/$role->id_role") }}"><i class="icon-remove icon-white"></i></a>
              </td>
		        </tr>			
			@endforeach

      </tbody>
    </table>
@else
	<p class="text-error">Sorry, no records found.</p>
@endif    
  
  <a href="{{ URL::to('admin/users/role/create') }}" class="btn btn-primary pull-left">Create a role</a>

@stop