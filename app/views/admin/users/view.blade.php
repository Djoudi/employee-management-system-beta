@extends('admin.tpl.master')

@section('title')
 View user record - Employee Management and Leave System
@stop

@section('content')

<div id="viewProfile">
	<div class="page-header">
		@if ( isset($records->first_name) && $records->first_name != "" )
		<h1>{{ $records->last_name }}, {{ $records->first_name }}</h1>
		<p>You are viewing the record of {{ $records->last_name }}, {{ $records->first_name }} </p>
		@endif
	</div>

	{{-- START THE VIEWING SECTION --}}
	<div id="viewWrap">	
        <div class="control-group well pull-left span8">
			<div class="pull-left">
				<h3 class="text-info">Personal Information</h3> <hr />
				@if ( isset($records->first_name) && $records->first_name != "" )
				<div class="controls">
					<p>{{ $records->title }} {{ $records->last_name }}, {{ $records->first_name }}, {{ $records->middle_name }}</p>
				</div>
				@endif

				@if ( $records->email )
				<div class="controls">
					<label>Email: </label>
					<p>{{ $records->email }}</p>			
				</div>
				@endif				

				@if ( $records->position )
					<div class="controls">
						<label for="position">Company Position: </label>
						<p>{{ $records->position }}</p>
					</div>
				@endif

				@if ( $records->mobile )
					<div class="controls">
						<label for="mobile">Mobile</label>
						<p>{{ $records->mobile }}</p>
					</div>	
				@endif
			</div>

			@if ( isset($records->street) && $records->street != "" )
			<div class="pull-right span4">
				<h3 class="text-info">Address</h3> <hr />
				<div class="controls">
					<address>
					  {{ $records->street }}<br>
					  {{ $records->town_city }} <br /> 
					  {{ $records->country }} {{ $records->postal }} <br />
					  @if ( $records->tel )
					  	<abbr title="Phone">Tel:</abbr> {{ $records->tel }}
					  @endif
					</address>					
				</div>							
			</div>
			<br class="clear" />
			@endif
        </div>

		@if ( isset($records->company_name) && $records->company_name != "" )
		<div class="control-group well pull-left span8">
			<div class="pull-left">
				<h3 class="text-info">Employment Details</h3> <hr />

				@if ( $records->company_name )
				<div class="controls">
					<label for="company_name">Company :</label>
					{{ $records->company_name }}
				</div>
				@endif

				@if ( $records->department )
					<div class="controls">
						<label for="department">Company department: </label>
						<p>{{ $records->department }}</p>
					</div>
				@endif

				@if ( $records->job_scope )
				<div class="controls">
					<label for="job_scope">Job Scope: </label>
					{{ $records->job_scope }}
				</div>
				@endif

				@if ( isset($records->employment_date) && $records->employment_date != "0000-00-00" )
				<div class="controls">
					<label for="employment_date">Employment Date: </label>
					<input class="input-block-level" type="text" value="{{ date("F j, Y", strtotime($records->employment_date)) }}" disabled>
				</div>
				@endif
				
				@if ( $records->employment_date_end != "0000-00-00" )
				<div class="controls">
					<label for="employment_date_end_picker">Employment Date End: </label>
					<input class="input-block-level" type="text" value="{{ date("F j, Y", strtotime($records->employment_date_end)) }}" disabled>
				</div>
				@endif

				@if ( $records->salary )
				<div class="controls">
					<label for="salary">Salary :</label>
					{{ $records->currency }} @if ( isset($records->salary) && $records->salary) {{ number_format($records->salary, 2); }} @endif
				</div>
				@endif

				@if ( $records->bank ) 
				<div class="controls">
					<label for="bank">Bank Name :</label>
					{{ $records->bank }}, {{ $records->bank_branch }}
				</div>
				@endif

				@if ( $records->account_name )
				<div class="controls">
					<label for="account_name">Account Name :</label>
					{{ $records->account_name }}
				</div>
				@endif

				@if ( $records->account_number )
				<div class="controls">
					<label for="account_number">Account Number :</label>
					{{ $records->account_number }}
				</div>
				@endif

				@if ( $records->account_type )
				<div class="controls">
					<label for="account_type">Account Type :</label>
					{{ $records->account_type }}
				</div>				
				@endif

				@if (  $records->notes )			
					<div class="controls">
						<label for="notes">Notes :</label>
						{{ $records->notes }}
					</div>
				@endif

			</div>

			@if ( $records->annual_leave )
			<div class="pull-right span4">
				<h3 class="text-info">Employee Leave Details</h3> <hr />
				<div class="controls">
					<label for="annual_leave">Annual Leaves left:</label>
					{{ $records->annual_leave }}
				</div>

				@if ( $records->sick_leave )
				<div class="controls">
					<label for="sick_leave">Sick Leaves left:</label>
					{{ $records->sick_leave }}
				</div>
				@endif

				@if ( $records->title == "Mrs.")
				<div class="controls">
					<label for="maternity_leave">Maternity Leaves left:</label>
					{{ $records->maternity_leave }}
				</div>
				@endif

				@if ( $records->title == "Mr." )
				<div class="controls">
					<label for="paternity_leave">Paternity Leaves left:</label>
					{{ $records->paternity_leave }}
				</div>
				@endif			
			</div>
			@endif
		</div>
		@endif

		<div class="controls pull-left clear">
			<a title="Edit" class="btn btn-primary pull-right" href="{{ URL::to("admin/users/update/$records->id_user") }}"><i class="icon-pencil icon-white"></i> Edit Information</a>	
		</div>
	</div>
</div>
@stop