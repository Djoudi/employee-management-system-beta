<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('department_users', function(Blueprint $table)
		{
			$table->increments('id_department_user');
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id_user')->on('users');
			$table->string('department_name');
			$table->integer('id_department')->unsigned();
			$table->foreign('id_department')->references('id_department')->on('departments');
			$table->string('department_role_name');
			$table->integer('id_department_role')->unsigned();
			$table->foreign('id_department_role')->references('id_department_role')->on('department_roles');			
			$table->string('name');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('department_users');
	}

}
