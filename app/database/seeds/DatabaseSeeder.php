<?php

class DatabaseSeeder extends Seeder {
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		
		$this->call('DepartmentsTableSeeder');
        $this->call('DepartmentRolesTableSeeder');
        $this->call('UsersRoleTableSeeder');
        $this->call('UsersTableSeeder');      
        $this->call('UsersLeaveTableSeeder');
        $this->call('DepartmentUsersTableSeeder');
        $this->call('SettingsTableSeeder');
	}

}

class DepartmentsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('departments')->delete();

        Departments::create(array(
        	'department_email'	=> 'john@doe.com',
        	'name' 				=> 'Information Technology',
        	'description' 		=> 'Deals with computer related infrastructure'
        ));
	}
}

class DepartmentRolesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('department_roles')->delete();

        DepartmentRoles::create(array(
        	'id_department' 	=> '1',
        	'name' 				=> 'Regular Employee',
        	'description' 		=> 'A regular employee'
        ));
	}
}

class UsersRoleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('users_role')->delete();

        UserRole::create(array(
        	'role' 				=> 'admin',
        	'role_description' 	=> 'admin role',
			'created_at'		=> '2000-01-01 00:00:00',
			'updated_at'		=> '2000-01-01 00:00:00'       	
        ));
	}
}

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('users')->delete();

        User::create(array(
        	'id_role' 				=> '1',
        	'department_name' 		=> 'Information Technology',
        	'department_role_name' 	=> 'Regular Employee',
        	'title' 				=> 'Mr.',
			'first_name' 			=> 'John',
			'middle_name' 			=> 'S',
			'last_name' 			=> 'Doe',
			'email' 				=> 'john@doe.com',
			'password' 				=> '$2a$10$2eJkbq95AHQse2aE50.9R.ECXbtLURIel/9DUK05IMsbq2dlVvRSq', //Testing123
			'mobile' 				=> '+123456789',
			'tel' 					=> '+987654321',
			'street' 				=> '#1 Tatalon St. University Village',
			'town_city' 			=> 'Las Pinas City',
			'postal' 				=> '1234',
			'country' 				=> 'Philippines',
			'company_name' 			=> 'Employee & Leave Management Services',
			'job_scope' 			=> 'Oversee people',			
			'employment_date' 		=> '2000-01-01',
			'employment_date_end' 	=> '2000-01-02',
			'salary' 				=> '',
			'bank' 					=> '',
			'account_name' 			=> '',
			'account_number' 		=> '',
			'account_type' 			=> '',
			'others' 				=> 'NULL',
			'currency' 				=> '',
			'annual_leave' 			=> '14',
			'sick_leave' 			=> '6',
			'maternity_leave' 		=> '',
			'paternity_leave' 		=> '1',
			'status' 				=> '1',			
			'created_at'			=> '2000-01-01 00:00:00',
			'updated_at'			=> '2000-01-01 00:00:00'
        ));
	}
}

class UsersLeaveTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('users_leave')->delete();

        UserLeave::create(array(
        	'id_user' 				=> '1',
        	'first_name' 			=> 'John',
        	'last_name' 			=> 'Doe',
        	'leave_type' 			=> 'annual',
        	'day_leave_type' 		=> 'oneday',
        	'day_leave' 			=> '2013-07-31',
        	'leave_time' 			=> 'NULL',
        	'date_from' 			=> '0000-00-00',
        	'date_to' 				=> '0000-00-00',
        	'number_of_days' 		=> '1',
        	'leave_details' 		=> 'Family ocassion',
        	'approval' 				=> '0',
        	'approved_by' 			=> '',
        	'approved_date' 		=> '',
        	'notes' 				=> '',
			'created_at'			=> '2000-01-01 00:00:00',
			'updated_at'			=> '2000-01-01 00:00:00'     	
        ));
	}
}

class DepartmentUsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('department_users')->delete();

        DepartmentUsers::create(array(
        	'department_name'		=> 'Information Technology',
        	'id_department' 		=> 1,
        	'department_role_name'	=> 'Regular Employee',
        	'id_department_role' 	=> 1,        	
        	'id_user' 				=> 1,
        	'name' 					=> 'Doe, John'
        ));
	}
}

class SettingsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('settings')->delete();

        Settings::create(array(
        	'name'	=> 'LEAVE_EMAIL',
        	'value'	=> 'someone@email.com'
        ));
	}
}